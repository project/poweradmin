<?php


/*******************************************************************************
  The PowerDNS Administration module
  Zone Management pages/forms
*******************************************************************************/

/**
 * List domains
 * @return unknown_type
 */
function poweradmin_page_domains_list() {
  module_load_include('inc', 'poweradmin', 'poweradmin.api');
  $zones = poweradmin_list_domains();

  $header = array(t("Name"), t("Master"), t("Records"), t("Type"), t("Actions"));
  $rows = array();

  foreach ($zones as $zone) {
    $rows[] = array(
      $zone->name,
      $zone->master,
      $zone->count_records,
      $zone->type,
      _poweradmin_link_showzone($zone->id) .' '.
      _poweradmin_link_editzone($zone->id) .' '.
      _poweradmin_link_editrecords($zone->id) .' '.
      _poweradmin_link_delzone($zone->id),
    );
  }

  return theme('table', $header, $rows) . _poweradmin_link_addzone();
}

/**
 * List records associated to a domain
 * @param int $domain_id
 * @return string
 */
function poweradmin_page_show_domain($domain_id) {
  module_load_include('inc', 'poweradmin', 'poweradmin.api');
  $zone = poweradmin_get_domain($domain_id);
  drupal_set_title(t("Zone: %name", array("%name" => $zone->name)));

  $zone_info = "<strong>". t("Type") .":</strong> ". $zone->type;
  if ($zone->master) {
    $zone_info .= " - <strong>". t("Master") .":</strong> ". $zone->master;
  }
  if ($zone->last_check) {
    $zone_info .= " - <strong>". t("Last check") .":</strong> ". $zone->last_check;
  }
  if ($zone->notified_serial) {
    $zone_info .= " - <strong>". t("Notified serial") .":</strong> ". $zone->notified_serial;
  }

  $zone_info = "<div>$zone_info</div>";

  $header = array(
    array('data' => t("Id"), 'width' => '40'),
    t("Name"),
    t("Type"),
    t("Content"),
    array('data' => t("Priority"), 'width' => '60'),
    array('data' => t("TTL"), 'width' => '60'),
    array('data' => t("Actions"), 'width' => '60'));
  $rows = array();

  foreach ($zone->records as $record_id => $record) {
    $rows[] = array(
      $record_id,
      $record->name,
      $record->type,
      $record->content,
      $record->prio,
      $record->ttl,
      _poweradmin_link_delrecord($record_id),
    );
  }
  return $zone_info . theme('table', $header, $rows) ."<div>". _poweradmin_link_addrecord($domain_id) ."</div>";
}

/**
 * Form to edit the data related to a domain
 * @param unknown_type $form_state
 * @param unknown_type $domain_id
 * @return unknown_type
 */
function poweradmin_domain_edit_info_form($form_state, $domain_id = NULL) {
  module_load_include('inc', 'poweradmin', 'poweradmin.api');
  if ($domain_id) {
    $zone = poweradmin_get_domain($domain_id);
    drupal_set_title(t("Edit zone: %name", array("%name" => $zone->name)));
  }
  else {
    $zone = new stdClass();
    drupal_set_title(t("Create new zone"));
  }

  $form = array();
  $form['domain_id'] = array(
    '#type' => 'value',
    '#value' => $domain_id,
  );
  $form['domain'] = array(
    '#type' => 'value',
    '#value' => $zone,
  );
  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t("Name"),
    '#default_value' => $zone->name,
    '#description' => t("The domain name of the zone."),
    '#required' => TRUE,
  );
  $form['type'] = array(
    '#type' => 'select',
    '#title' => t("Type"),
    '#default_value' => $zone->type,
    '#options' => _poweradmin_dtypes(),
    '#required' => TRUE,
  );
  $form['master'] = array(
    '#type' => 'textfield',
    '#title' => t("Master"),
    '#default_value' => $zone->master,
    '#description' => t("The master nameserver name for a slave zone."),
  );
  $form['last_check'] = array(
    '#type' => 'textfield',
    '#title' => t("Last check"),
    '#default_value' => $zone->last_check,
  );
  $form['notified_serial'] = array(
    '#type' => 'textfield',
    '#title' => t("Notified serial"),
    '#default_value' => $zone->notified_serial,
  );
  $form['submit'] = array('#type' => 'submit', '#value' => t("Save Zone"));
  return $form;
}

/**
 * Domain info editing form - submit
 * @param unknown_type $form
 * @param unknown_type $form_state
 * @return unknown_type
 */
function poweradmin_domain_edit_info_form_submit($form, &$form_state) {
  module_load_include('inc', 'poweradmin', 'poweradmin.api');
  $domain_id = $form_state['values']['domain_id'];
  $orig_domain = $form_state['values']['domain'];
  $definition = new stdClass();
  $definition->name = $form_state['values']['name'];
  $definition->type = $form_state['values']['type'];
  $definition->master = $form_state['values']['master'];
  $definition->last_check = $form_state['values']['last_check'];
  $definition->notified_serial = $form_state['values']['notified_serial'];
  if ($domain_id) {
    // Update zone
    poweradmin_update_domain($domain_id, $definition);
  }
  else {
    // Add zone
    poweradmin_insert_domain($definition);
    $domain_id = $definition->id;
  }
  $form_state['redirect'] = "admin/poweradmin/zones/". $domain_id;
}

/**
 * Edit the records associated to a zone.
 * @param array $form_state
 * @param int $domain_id
 * @return array
 */
function poweradmin_domain_edit_records_form($form_state, $domain_id = NULL) {
  module_load_include('inc', 'poweradmin', 'poweradmin.api');
  $zone = poweradmin_get_domain($domain_id);
  drupal_set_title(t("Edit records for: %name", array("%name" => $zone->name)));
  if (!$zone) {
    drupal_not_found();
    return;
  }

  $form = array();

  $form['zone'] = array(
    '#type' => 'value',
    '#value' => $zone,
  );

  $form['records'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#title' => t("Zone records"),
  );

  foreach ($zone->records as $record) {
    $form['records'][$record->id] = array(
      '#type' => 'fieldset',
    );
    $form['records'][$record->id]['name'] = array(
      '#type' => 'textfield',
      '#title' => t('Name'),
      '#default_value' => $record->name,
      '#required' => TRUE,
    );
    $form['records'][$record->id]['type'] = array(
      '#type' => 'select',
      '#title' => t('Type'),
      '#options' => _poweradmin_rtypes(),
      '#default_value' => $record->type,
      '#required' => TRUE,
    );
    $form['records'][$record->id]['content'] = array(
      '#type' => 'textfield',
      '#title' => t('Content'),
      '#default_value' => $record->content,
      '#required' => TRUE,
    );
    $form['records'][$record->id]['prio'] = array(
      '#type' => 'textfield',
      '#title' => t('Priority'),
      '#default_value' => $record->prio,
    );
    $form['records'][$record->id]['ttl'] = array(
      '#type' => 'textfield',
      '#title' => t('TTL'),
      '#default_value' => $record->ttl,
    );

    $rows[] = array(

      $record->name,
      $record->type,
      $record->content,

      $record->prio,
      $record->ttl,
      "",
    );
  }

  if ($domain_id) {
    $form['links'] = array(
      '#type' => 'markup',
      '#value' => _poweradmin_link_addrecord($domain_id),
      '#prefix' => '<div>',
      '#suffix' => '</div>',
    );
  }

  $form['submit'] = array('#type' => 'submit', '#value' => t("Save Records"));

  return $form;
}

/**
 * Edit zone submit handler
 * @param array $form
 * @param array $form_state
 * @return void
 */
function poweradmin_domain_edit_records_form_submit($form, &$form_state) {
  module_load_include('inc', 'poweradmin', 'poweradmin.api');
  $zone = $form_state['values']['zone'];
  $records_to_update = array();
  // Build the list of necessary updates
  foreach ($zone->records as $id => $or) {
    $newrecord = $form_state['values']['records'][$id];
    foreach (array('name', 'type', 'content', 'prio', 'ttl') as $key) {
      $newvalue = $newrecord[$key];
      if ($newvalue != $or->$key) {
        $records_to_update[$id][$key] = $newvalue;
      }
    }
  }
  // Update all records that needs to
  foreach ($records_to_update as $record_id => $definition) {
    drupal_set_message(t("Updating record !name", array("!name" => $record_id)));
    poweradmin_update_record($record_id, $definition);
  }
  drupal_set_message(t("Updated !count records", array('!count' => count($records_to_update))));
}

/**
 * Delete zone form
 * @param array $form_state
 * @param int $domain_id
 * @return array
 */
function poweradmin_domain_delete_form($form_state, $domain_id) {
  module_load_include('inc', 'poweradmin', 'poweradmin.api');
  $zone = poweradmin_get_domain($domain_id);
  drupal_set_title(t("Delete zone: %name", array("%name" => $zone->name)));
  $form = array();
  $form['domain_id'] = array('#type' => 'value', '#value' => $domain_id);
  $form['message'] = array(
    '#type' => 'markup',
    '#value' => t("Are you sure to permanently delete zone %name?", array('%name' => $zone->name)),
    '#prefix' => '<div>', '#suffix' => '</div>');
  $form['confirm'] = array('#type' => 'submit', '#value' => t("Delete"));
  return $form;
}

/**
 * Delete zone submit handler
 * @param array $form
 * @param array $form_state
 * @return void
 */
function poweradmin_domain_delete_form_submit($form, &$form_state) {
  module_load_include('inc', 'poweradmin', 'poweradmin.api');
  $domain_id = $form_state['values']['domain_id'];
  $zone = poweradmin_get_domain($domain_id);
  poweradmin_delete_domain($domain_id);
  drupal_set_message(t("Deleted domain !name", array('!name' => $zone->name)));
  $form_state['redirect'] = 'admin/poweradmin/zones';
}

/**
 * Themeize the zone editing form
 * @param $zone
 * @return array
 */
function theme_poweradmin_domain_edit_records_form($form) {
  $header = array(
    array('data' => t("Id"), 'width' => '40'),
    t("Name"),
    t("Type"),
    t("Content"),
    array('data' => t("Priority"), 'width' => '60'),
    array('data' => t("TTL"), 'width' => '60'),
    array('data' => t("Actions"), 'width' => '60'));
  $rows = array();

  foreach ($form['records'] as $domain_id => $zone) {
    if (is_numeric($domain_id)) {
      $zone['name']['#size'] = 30;
      $zone['name']['#title'] = NULL;
      $zone['type']['#title'] = NULL;
      $zone['content']['#size'] = 30;
      $zone['content']['#title'] = NULL;
      $zone['prio']['#size'] = '8';
      $zone['prio']['#title'] = NULL;
      $zone['ttl']['#size'] = '10';
      $zone['ttl']['#title'] = NULL;
      $rows[] = array(
        $domain_id,
        drupal_render($zone['name']),
        drupal_render($zone['type']),
        drupal_render($zone['content']),
        drupal_render($zone['prio']),
        drupal_render($zone['ttl']),
        _poweradmin_link_delrecord($domain_id),
      );
    }
  }

  unset($form['records']);
  return theme('table', $header, $rows) . drupal_render($form);
}

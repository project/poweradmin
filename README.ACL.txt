
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

                         PowerAdmin ACLs Mechanism Plan

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  WARNING: No ACLs Supported at the moment (TODO)
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

ACL mechanism:
  - global permissions
  - the user who creates a zone is the zone owner
  - the zone owner can grant permissions to other users
  - there could be per-record access granting

~~~~~~~~~~~~~~~~~~~~~~
  GLOBAL PERMISSIONS
~~~~~~~~~~~~~~~~~~~~~~
  poweradmin.superuser -> gives the user maximum access on PowerAdmin
  poweradmin.user -> let the user access PowerAdmin
  
  poweradmin.zone.create.master
  poweradmin.zone.create.slave
  poweradmin.zone.view.own
  poweradmin.zone.view.any
  poweradmin.zone.edit.own
  poweradmin.zone.edit.any
  poweradmin.zone.delete.own
  poweradmin.zone.delete.any

~~~~~~~~~~~~~~~~~~~~
  ZONE PERMISSIONS
~~~~~~~~~~~~~~~~~~~~
  poweradmin.zone.owner -> defines the zone owner (GRANT WITH CARE!)
  poweradmin.zone.view -> grant whole view permissions on the zone
  poweradmin.zone.edit -> grant whole edit permissions on the zone
  poweradmin.zone.delete-> grant whole delete permissions on the zone
  poweradmin.zone.info.view -> view zone informations
  poweradmin.zone.info.edit -> edit zone informations
  poweradmin.zone.records.view.own -> view own records
  poweradmin.zone.records.view.any -> view any records
  poweradmin.zone.records.create -> create records
  poweradmin.zone.records.edit.own -> edit own records
  poweradmin.zone.records.edit.any -> edit any records
  poweradmin.zone.records.delete.own -> delete own records
  poweradmin.zone.records.delete.any -> delete any records


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
INSTALLATION
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  1. Install PowerDNS
  2. Install Drupal and this module
  3. Configure PowerDNS
  4. Start PowerDNS
  5. Enjoy

~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
IMPORTANT NOTE
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  Do not use table prefixes when installing this module, since PowerDNS
  doesn't support it. Tables MUST have the correct unprefixed name.
  (But, you can configure per-table prefixes in the settings.php file).
  
  Woah! Fixed that! U could use whatever prefix you want (maybe..)


## Bash script to remove symbols from paths
## (thanks god, sed exists..)
for file in $( find -type f -regex ".*\.\(module\|inc\|install\|tpl\|tpl\.php\)" ); do
  sed -i "s#POWERADMIN_ADMIN_ROOT \.\"#\"admin/poweradmin#" "$file"
done

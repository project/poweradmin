<?php


/*******************************************************************************
  The PowerDNS Administration module - APIs
********************************************************************************

  These functions are intended to be used to run all the queryes on
  the PowerDNS tables.

*******************************************************************************/

/**
 * Returns a list of all defined domains
 * @return array
 */
function poweradmin_list_domains() {
  $res = db_query("SELECT d.*, COUNT(r.id) AS count_records
    FROM {pdns_domains} d LEFT JOIN {pdns_records} r ON r.domain_id = d.id
    GROUP BY d.name, d.id, d.type ORDER BY d.name");
  return _poweradmin_dbres_to_array($res, 'id');
}

/**
 * Get a domain definition, plus related zones
 * @param int $domain_id
 * @return array
 */
function poweradmin_get_domain($domain_id) {
  $res = db_query_range("SELECT * FROM {pdns_domains} WHERE id=%d", $domain_id, 0, 1);
  $domain = db_fetch_object($res);
  $domain->records = poweradmin_list_records($domain_id);
  return $domain;
}

/**
 * Insert a domain definition
 * @param object $definition
 * @return bool
 */
function poweradmin_insert_domain(&$definition) {
  return drupal_write_record('pdns_domains', $definition);
}

/**
 * Update a domain definition
 * @param int $domain_id
 * @param object $definition
 * @return bool
 */
function poweradmin_update_domain($domain_id, &$definition) {
  $definition = (object)$definition;
  $definition->id = $domain_id;
  return drupal_write_record('pdns_domains', $definition, 'id');
}

/**
 * Update the SOA serial
 * taken from poweradmin app
 * TODO check this
 * @param unknown_type $domain_id
 * @return unknown_type
 */
function poweradmin_update_soa_serial($domain_id) {
  $notified_serial = db_result(db_query_range("SELECT notified_serial FROM {pdns_domains} WHERE id = %d", $domain_id, 0, 1));
  $result = db_result(db_query_range("SELECT content FROM {pdns_records} WHERE type = 'SOA' AND domain_id = %d", $domain_id, 0, 1));
  $need_to_update = false;

  // Split content of current SOA record into an array.
  $soa = explode(" ", $result);

  // Check if we have to update the serial field.
  //
  // The serial should be updated, unless:
  //  - the serial is set to "0", see /Documentation/DNS-SOA#PowerDNSspecifics on
  //    the Poweradmin website
  //  - the serial is set to YYYYMMDD99, it's RFC 1912 style already and has
  //    reached it limit of revisions for today

  if ($soa[2] == "0") {
    return true;
  }
  elseif ($soa[2] == date('Ymd') . "99") {
    return true;
  }
  else {
    $today = date('Ymd');

    // Determine revision.
    if (strncmp($today, $soa[2], 8) === 0) {
      // Current serial starts with date of today, so we need to update
      // the revision only. To do so, determine current revision first,
      // then update counter.
      $revision = (int) substr($soa[2], -2);
      ++$revision;
    }
    else {
      // Current serial did not start of today, so it's either an older
      // serial or a serial that does not adhere the recommended syntax
      // of RFC-1912. In either way, set a fresh serial
      $revision = "00";
    }

    $serial = $today . str_pad($revision, 2, "0", STR_PAD_LEFT);

    // Change serial in SOA array.
    $soa[2] = $serial;

    // Build new SOA record content and update the database.
    $content = "";
    for ($i = 0; $i < count($soa); $i++) {
      $content .= $soa[$i] . " ";
    }
    db_query("UPDATE {pdns_records} SET content = '%s' WHERE domain_id = %s AND type = 'SOA'", $content, $domain_id);
    return true;
  }
}


/**
 * Delete a domain and all its related records
 * @param int $domain_id
 * @return void
 */
function poweradmin_delete_domain($domain_id) {
  db_query("DELETE FROM {pdns_records} WHERE domain_id=%d", $domain_id);
  db_query("DELETE FROM {pdns_domains} WHERE id=%d", $domain_id);
  db_query("DELETE FROM {pdns_domains_users} WHERE domain_id=%d", $domain_id);
}

/**
 * Add a domain ownership relation
 * @param int $domain_id
 * @param int $uid
 * @return void
 */
function poweradmin_add_domain_owner($domain_id, $uid) {
  $exists = db_result(db_query("SELECT count(*) FROM {pdns_domains_users} WHERE domain_id=%d AND uid=%d", $domain_id, $uid));
  if (!$exists) {
    db_query("INSERT INTO {pdns_domains_users} (domain_id, uid) VALUES (%d, %d)", $domain_id, $uid);
  }
}

/**
 * Delete a domain ownership relation
 * @param int $domain_id
 * @param int $uid
 * @return void
 */
function poweradmin_del_domain_owner($domain_id, $uid) {
  db_query("DELETE FROM {pdns_domains_users} WHERE domain_id=%d AND uid=%d", $domain_id, $uid);
}

/**
 * List all records associated to a given domain
 * @param int $domain_id
 * @return void
 */
function poweradmin_list_records($domain_id) {
  $res = db_query("SELECT * FROM {pdns_records} WHERE domain_id=%d", $domain_id);
  return _poweradmin_dbres_to_array($res, 'id');
}

/**
 * Get a record by id
 * @param unknown_type $record_id
 * @return unknown_type
 */
function poweradmin_get_record($record_id) {
  $res = db_query_range("SELECT * FROM {pdns_records} WHERE id=%d", $record_id, 0, 1);
  return db_fetch_object($res);
}

/**
 * Write a record definition
 * @param unknown_type $definition
 * @return unknown_type
 */
function poweradmin_insert_record(&$definition) {
  $definition = (object)$definition;
  $res = drupal_write_record('pdns_records', $definition);
  poweradmin_update_soa_serial($definition->domain_id);
  return $res;
}

/**
 * Update a record
 * @param unknown_type $record_id
 * @param unknown_type $definition
 * @return unknown_type
 */
function poweradmin_update_record($record_id, &$definition) {
  $definition = (object)$definition;
  $definition->id = $record_id;
  $res = drupal_write_record('pdns_records', $definition, 'id');
  $record = poweradmin_get_record($record_id);
  poweradmin_update_soa_serial($record->domain_id);
  return $res;
}

/**
 * Delete a record
 * @param unknown_type $record_id
 * @return unknown_type
 */
function poweradmin_delete_record($record_id) {
  db_query("DELETE FROM {pdns_records} WHERE id=%d", $record_id);
  //TODO update SOA serial
  //poweradmin_update_soa_serial($definition->domain_id);
}

/**
 * Get the list of all supermasters
 * @return unknown_type
 */
function poweradmin_list_supermasters() {
  $res = db_query("SELECT * FROM {pdns_supermasters}");
  return _poweradmin_dbres_to_array($res);
}

/**
 * Insert a new supermaster
 * @param unknown_type $sm_ip
 * @param unknown_type $sm_ns
 * @return unknown_type
 */
function poweradmin_insert_supermaster($sm_ip, $sm_ns) {
  db_query("INSERT INTO {pdns_supermasters} (ip, nameserver) VALUES ('%s', '%s')", $sm_ip, $sm_ns);
}

/**
 * Delete a supermaster
 * @param unknown_type $sm_ip
 * @param unknown_type $sm_ns
 * @return unknown_type
 */
function poweradmin_delete_supermaster($sm_ip, $sm_ns) {
  db_query("DELETE FROM {pdns_supermasters} WHERE ip='%s' AND nameserver='%s'", $sm_ip, $sm_ns);
}

/**
 * Generate configuration file for PowerDNS
 * @return string
 */
function poweradmin_generate_pdns_conf() {
  global $db_url, $db_type;
  $db_conf = parse_url($db_url);
  $date = format_date(time(), 'custom', 'Y-m-d H:i');
  $dbpath = explode('/', $db_conf['path']);
  $dbname = $dbpath[1];
  $dbtype = '';
  if ($db_type == 'pgsql') {
    $dbtype = 'gpgsql';
  }
  else {
    $dbtype = 'gmysql';
  }

  $table_domains = db_prefix_tables('{pdns_domains}');
  $table_records = db_prefix_tables('{pdns_records}');
  $table_supermasters = db_prefix_tables('{pdns_supermasters}');

  $out  = "## /etc/powerdns/pdns.conf\n";
  $out .= "## ------------------------------------------------------------\n";
  $out .= "## ". t("Autogenerated by Drupal PowerAdmin Module") ."\n";
  $out .= "## ". t("Date") .": $date\n";
  $out .= "## ------------------------------------------------------------\n";
  $out .= "\n";
  $out .= "## ------------------------------------------------------------\n";
  $out .= "## ". t("User/group to run as") ."\n";
  $out .= "## ------------------------------------------------------------\n";
  $out .= "setgid=pdns\n";
  $out .= "setuid=pdns\n";
  $out .= "\n";
  $out .= "## ------------------------------------------------------------\n";
  $out .= "## ". t("Backend database configuration") ."\n";
  $out .= "## ------------------------------------------------------------\n";
  $out .= "launch=$dbtype\n";
  $out .= "gmysql-host=$db_conf[host]\n";
  $out .= "gmysql-user=$db_conf[user]\n";
  $out .= "gmysql-password=$db_conf[pass]\n";
  $out .= "gmysql-dbname=$dbname\n";
  $out .= "\n";
  $out .= "## ------------------------------------------------------------\n";
  $out .= "## ". t("Redefine queries to use new table names") ."\n";
  $out .= "## ------------------------------------------------------------\n";
  $out .= "${dbtype}-basic-query=select content,ttl,prio,type,domain_id,name from $table_records where type='%s' and name='%s'\n";
  $out .= "${dbtype}-id-query=select content,ttl,prio,type,domain_id,name from $table_records where type='%s' and name='%s' and domain_id=%d\n";
  $out .= "${dbtype}-any-query=select content,ttl,prio,type,domain_id,name from $table_records where name='%s'\n";
  $out .= "${dbtype}-any-id-query=select content,ttl,prio,type,domain_id,name from $table_records where name='%s' and domain_id=%d\n";
  $out .= "${dbtype}-list-query=select content,ttl,prio,type,domain_id,name from $table_records where domain_id=%d\n";
  $out .= "${dbtype}-master-zone-query=SELECT master FROM $table_domains WHERE name='%s' AND type='SLAVE'\n";
  $out .= "${dbtype}-info-zone-query=SELECT id,name,master,last_check,notified_serial,type FROM $table_domains WHERE name='%s'\n";
  $out .= "${dbtype}-info-all-slaves-query=SELECT id,name,master,last_check,type FROM $table_domains WHERE type='SLAVE'\n";
  $out .= "${dbtype}-supermaster-query=SELECT account FROM $table_supermasters WHERE ip='%s' AND nameserver='%s');\n";
  $out .= "${dbtype}-insert-slave-query=INSERT INTO $table_domains (type,name,master,account) VALUES ('SLAVE','%s','%s','%s')\n";
  $out .= "${dbtype}-insert-record-query=INSERT INTO $table_records (content,ttl,prio,type,domain_id,name) VALUES ('%s',%d,%d,'%s',%d,'%s')\n";
  $out .= "${dbtype}-update-serial-query=UPDATE $table_domains SET notified_serial=%d WHERE id=%d\n";
  $out .= "${dbtype}-update-lastcheck-query=UPDATE $table_domains SET notified_serial=%d WHERE id=%d\n";
  $out .= "${dbtype}-info-all-master-query=SELECT id,name,master,last_check,notified_serial,type FROM $table_domains WHERE type='MASTER'\n";
  $out .= "${dbtype}-delete-zone-query=DELETE FROM $table_records WHERE domain_id=%d\n";
  $out .= "\n";
  $out .= "## ------------------------------------------------------------\n";
  $out .= "## ". t("Fancy records") ."\n";
  $out .= "## ------------------------------------------------------------\n";
  $out .= "# wildcard-query=SELECT content,ttl,prio,type,domain_id,name FROM $table_records WHERE type='%s' AND name LIKE '%s'\n";
  $out .= "# wildcard-id-query=SELECT content,ttl,prio,type,domain_id,name FROM $table_records WHERE type='%s' AND name LIKE '%s' AND domain_id=%d\n";
  $out .= "# wildcard-any-query=SELECT content,ttl,prio,type,domain_id,name FROM $table_records WHERE name LIKE '%s'\n";
  $out .= "# wildcard-any-id-query=SELECT content,ttl,prio,type,domain_id,name FROM $table_records WHERE name LIKE '%s' AND domain_id=%d\n";
  $out .= "\n";
  $out .= "## ------------------------------------------------------------\n";
  $out .= "## ". t("Recursion (\"forwarders\")") ."\n";
  $out .= "## ------------------------------------------------------------\n";
  $out .= "# allow-recursion=0.0.0.0/0\n";
  $out .= "# recursor=x.x.x.x y.y.y.y\n";
  $out .= "\n";
  $out .= "## ------------------------------------------------------------\n";
  $out .= "## ". t("Uncomment the following lines to enable webserver") ."\n";
  $out .= "## ------------------------------------------------------------\n";
  $out .= "# webserver=yes\n";
  $out .= "# webserver-address=127.0.0.1\n";
  $out .= "# webserver-password=my_pass\n";
  $out .= "# webserver-port=8081\n";

  return $out;
}




/**
 * Fetch all the rows in a database result as objects
 * @param mysql_result $res
 * @param string $key field to be used as key
 * @return array
 */
function _poweradmin_dbres_to_array($res, $key = NULL) {
  $rows = array();
  while ($row = db_fetch_object($res)) {
    if (!$key) {
      $rows[] = $row;
    }
    else {
      $rows[$row->$key] = $row;
    }
  }
  return $rows;
}






/**
 * Get the path of an image in the images directory of this module
 * @param string $imagename the image name
 * @return string containing the full path to image
 */
function _poweradmin_get_imagepath($imagename) {
  return drupal_get_path('module', 'poweradmin') ."/images/${imagename}.png";
}

/**
 * Create an icon link
 * @param string $icon_name
 * @param string $text
 * @param string $path
 * @return string
 */
function _poweradmin_iconlink($icon_name, $text, $path, $addtext = FALSE) {
  $icon = theme('image', _poweradmin_get_imagepath($icon_name), $text, $text);
  if ($addtext) {
    $icon .= " ". $text;
  }
  return l($icon, $path, array('html' => TRUE));
}

function _poweradmin_link_addzone($text = TRUE) {
  return _poweradmin_iconlink('add', t("Add Zone"), "admin/poweradmin/zones/add", TRUE);
}

function _poweradmin_link_addrecord($domain_id, $text = TRUE) {
  return _poweradmin_iconlink('add', t("Add Record"), "admin/poweradmin/records/add/$domain_id", $text);
}

function _poweradmin_link_showzone($domain_id) {
  return _poweradmin_iconlink('application_view_list', t("Show Zone"), "admin/poweradmin/zones/$domain_id");
}

function _poweradmin_link_editzone($domain_id) {
  return _poweradmin_iconlink('application_edit', t("Edit Zone"), "admin/poweradmin/zones/$domain_id/edit");
}

function _poweradmin_link_editrecords($domain_id) {
  return _poweradmin_iconlink('application_form_edit', t("Edit Records"), "admin/poweradmin/zones/$domain_id/records");
}

function _poweradmin_link_delzone($id) {
  return _poweradmin_iconlink('delete', t("Delete Zone"), "admin/poweradmin/zones/$id/delete");
}

function _poweradmin_link_delrecord($id) {
  return _poweradmin_iconlink('delete', t("Delete Record"), "admin/poweradmin/records/$id/delete");
}





/**
 * All the supported record types
 * @return array
 */
function _poweradmin_rtypes() {
  return array(
    'A' => 'A',
    'AAAA' => 'AAAA',
    'CNAME' => 'CNAME',
    'HINFO' => 'HINFO',
    'MX' => 'MX',
    'NAPTR' => 'NAPTR',
    'NS' => 'NS',
    'PTR' => 'PTR',
    'SOA' => 'SOA',
    'SPF' => 'SPF',
    'SRV' => 'SRV',
    'SSHFP' => 'SSHFP',
    'TXT' => 'TXT',
    'URL' => 'URL',
    'MBOXFW' => 'MBOXFW',
    'CURL' => 'CURL',
  );
}

/**
 * Domain types
 * @return unknown_type
 */
function _poweradmin_dtypes() {
  return array(
    "MASTER" => "MASTER",
    "SLAVE" => "SLAVE",
    "NATIVE" => "NATIVE",
  );
}

/**
 * Valid 1st level domains
 * @return unknown_type
 */
function _poweradmin_valid_tlds() {
  return array(
  "ac", "ad", "ae", "aero", "af", "ag", "ai", "al", "am", "an", "ao", "aq", "ar",
  "arpa", "as", "asia", "at", "au", "aw", "ax", "az", "ba", "bb", "bd", "be",
  "bf", "bg", "bh", "bi", "biz", "bj", "bm", "bn", "bo", "br", "bs", "bt", "bv",
  "bw", "by", "bz", "ca", "cat", "cc", "cd", "cf", "cg", "ch", "ci", "ck", "cl",
  "cm", "cn", "co", "com", "coop", "cr", "cu", "cv", "cx", "cy", "cz", "de", "dj",
  "dk", "dm", "do", "dz", "ec", "edu", "ee", "eg", "er", "es", "et", "eu", "fi",
  "fj", "fk", "fm", "fo", "fr", "ga", "gb", "gd", "ge", "gf", "gg", "gh", "gi",
  "gl", "gm", "gn", "gov", "gp", "gq", "gr", "gs", "gt", "gu", "gw", "gy", "hk",
  "hm", "hn", "hr", "ht", "hu", "id", "ie", "il", "im", "in", "info", "int", "io",
  "iq", "ir", "is", "it", "je", "jm", "jo", "jobs", "jp", "ke", "kg", "kh", "ki",
  "km", "kn", "kp", "kr", "kw", "ky", "kz", "la", "lb", "lc", "li", "lk", "lr",
  "ls", "lt", "lu", "lv", "ly", "ma", "mc", "md", "me", "mg", "mh", "mil", "mk",
  "ml", "mm", "mn", "mo", "mobi", "mp", "mq", "mr", "ms", "mt", "mu", "museum",
  "mv", "mw", "mx", "my", "mz", "na", "name", "nc", "ne", "net", "nf", "ng", "ni",
  "nl", "no", "np", "nr", "nu", "nz", "om", "org", "pa", "pe", "pf", "pg", "ph",
  "pk", "pl", "pm", "pn", "pr", "pro", "ps", "pt", "pw", "py", "qa", "re", "ro",
  "rs", "ru", "rw", "sa", "sb", "sc", "sd", "se", "sg", "sh", "si", "sj", "sk",
  "sl", "sm", "sn", "so", "sr", "st", "su", "sv", "sy", "sz", "tc", "td", "tel",
  "tf", "tg", "th", "tj", "tk", "tl", "tm", "tn", "to", "tp", "tr", "travel",
  "tt", "tv", "tw", "tz", "ua", "ug", "uk", "um", "us", "uy", "uz", "va", "vc",
  "ve", "vg", "vi", "vn", "vu", "wf", "ws", "xn--0zwm56d", "xn--11b5bs3a9aj6g",
  "xn--80akhbyknj4f", "xn--9t4b11yi5a", "xn--deba0ad", "xn--g6w251d",
  "xn--hgbk6aj7f53bba", "xn--hlcj6aya9esc7a", "xn--jxalpdlp", "xn--kgbechtv",
  "xn--zckzah", "ye", "yt", "yu", "za", "zm", "zw");
}

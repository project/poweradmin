<?php


/*******************************************************************************
  The PowerDNS Administration module
  Record Management pages/forms
*******************************************************************************/

/**
 * Record adding form
 * @param array $form_state
 * @param int $domain_id
 * @return array
 */
function poweradmin_record_add_form($form_state, $domain_id = NULL) {
  module_load_include('inc', 'poweradmin', 'poweradmin.api');
  $zone = poweradmin_get_domain($domain_id);
  drupal_set_title(t("Add record for domain %domain", array('%domain' => $zone->name)));

  $form = array();

  $form['domain_id'] = array(
    '#type' => 'textfield',
    '#value' => $domain_id,
  );

  $form['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#required' => TRUE,
  );
  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Type'),
    '#options' => _poweradmin_rtypes(),
    '#required' => TRUE,
    '#default_value' => 'A',
  );
  $form['content'] = array(
    '#type' => 'textfield',
    '#title' => t('Content'),
    '#required' => TRUE,
  );
  $form['prio'] = array(
    '#type' => 'textfield',
    '#title' => t('Priority'),
    '#default_value' => '0',
  );
  $form['ttl'] = array(
    '#type' => 'textfield',
    '#title' => t('TTL'),
    '#default_value' => '604800',
  );

  $form['submit'] = array('#type' => 'submit', '#value' => t("Save Record"));
  return $form;
}

/**
 * Submit handler for record adding form
 * @param array $form
 * @param array $form_state
 * @return void
 */
function poweradmin_record_add_form_submit($form, &$form_state) {
  module_load_include('inc', 'poweradmin', 'poweradmin.api');
  $record = new stdClass();
  $record->domain_id = $form_state['values']['domain_id'];
  $record->name = $form_state['values']['name'];
  $record->type = $form_state['values']['type'];
  $record->content = $form_state['values']['content'];
  $record->prio = $form_state['values']['prio'];
  $record->ttl = $form_state['values']['ttl'];
  dpm($record);
  poweradmin_insert_record($record);
  $zone = poweradmin_get_domain($record->domain_id);
  drupal_set_message(t('Added new record for zone !name', array('!name' => $zone->name)));
  $form_state['redirect'] = 'admin/poweradmin/zones/'. $record->domain_id;
}

/**
 * Delete record form
 * @param array $form_state
 * @return array
 */
function poweradmin_record_delete_form($form_state, $record_id) {
  module_load_include('inc', 'poweradmin', 'poweradmin.api');
  $record = poweradmin_get_record($record_id);
  drupal_set_title(t("Delete record: %name", array("%name" => $record->name .' '. $record->type .' '. $record->content)));
  $form = array();
  $form['record_id'] = array('#type' => 'value', '#value' => $record_id);
  $form['message'] = array(
    '#type' => 'markup',
    '#value' => t("Are you sure to permanently delete record \"%name\"?", array("%name" => $record->name .' '. $record->type .' '. $record->content)),
    '#prefix' => '<div>', '#suffix' => '</div>');
  $form['confirm'] = array('#type' => 'submit', '#value' => t("Delete"));
  return $form;
}

/**
 * Delete record form submit callback
 * @param array $form
 * @param array $form_state
 * @return void
 */
function poweradmin_record_delete_form_submit($form, &$form_state) {
  module_load_include('inc', 'poweradmin', 'poweradmin.api');
  $record_id = $form_state['values']['record_id'];
  $record = poweradmin_get_record($record_id);
  poweradmin_delete_record($record_id);
  drupal_set_message(t("Deleted record: %name", array("%name" => $record->name .' '. $record->type .' '. $record->content)));
  $form_state['redirect'] = 'admin/poweradmin/zones/'. $record->domain_id;
}
